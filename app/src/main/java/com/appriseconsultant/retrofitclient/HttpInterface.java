package com.appriseconsultant.retrofitclient;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface HttpInterface {

      @GET("posts")
      public Call<List<PostModel>> getPosts();
}
