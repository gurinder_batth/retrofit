package com.appriseconsultant.retrofitclient;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    String baseUrl = "https://jsonplaceholder.typicode.com";

    TextView textView;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textview);
        progress = (ProgressBar) findViewById(R.id.progress);
        fetchPosts();
    }

    public void fetchPosts(){

        progress.setVisibility(View.VISIBLE);

        Retrofit.Builder builder = new Retrofit.Builder();
        Retrofit retrofit =   builder.baseUrl(baseUrl)
                              .addConverterFactory( GsonConverterFactory.create() )
                              .build();

        HttpInterface httpInterface = retrofit.create( HttpInterface.class );
        Call<List<PostModel>> call =  httpInterface.getPosts();
        call.enqueue(new Callback<List<PostModel>>() {
            @Override
            public void onResponse(Call<List<PostModel>> call, Response<List<PostModel>> response) {

                       progress.setVisibility(View.GONE);
                       if( response.isSuccessful() == false ){
                           Toast.makeText(MainActivity.this, "Oops! Something went Wrong", Toast.LENGTH_SHORT).show();
                           return ;
                       }

                       List<PostModel> postModelList = response.body();

                       String message = "";

                       for( PostModel postModel : postModelList ){

                           message  = message + " Id = " + postModel.getId() + "\n";
                           message += "Title -> " + postModel.getTitle() + " \n ";
                           message +=  "Body -> "  + postModel.getBody() + " \n \n";

                       }

                       textView.setText( message );




            }

            @Override
            public void onFailure(Call<List<PostModel>> call, Throwable t) {
                progress.setVisibility(View.GONE);
            }
        });






    }
}